# 1/2 journée auto-formation
# RPG Combat Kata


Cette demi-journée peut être faite seule ou en binome.

L'objectif n'est pas de tout terminer, mais d'avoir le code le plus "clean" possible (pensez à l'image sur le nombre de WTF/minute), 
Ca n'est pas grave de ne pas finir.

Concentrez vous sur les 4 rules of Simple Design, écrivez des tests, faites attentions au nommage, à la duplication...

## Récupération du repo

```
git clone https://gitlab.com/ludopradel/cda-2020
```

## Création d'une branche 

```
git checkout -b nom.prenom
```

## Commits

Ajoutez des commits aussi souvent que nécessaire, pour me montrer le code intermédiaire.

Le message du commit doit être clair (pour vous et pour les personnes qui vont le relire).

Lorsque vous faites un commit, les tests sont aux verts. 

(Pensez au cycle du TDD avec commit : Red -> Green -> Refactor -> Commit)

## Push

```
git push origin nom-prenom
```

## Sujet

### Domaine

Dans ce kata, vous allez implémenter les mécanismes de combat d'un jeu de role (dégats / soin).


### Avant de commencer

* Essayez de ne pas lire les étapes en avance.
* Faites une étape à la fois, l'objectif est d'apprendre à travailler de manière incrémentale.

### Etape 1

- On peut connaitre le nombre de points de vie d'un personnage.
- Lorsqu'il est créé, un personnage commence avec 100 points de vie.

#### Tips

* Vous pouvez créer une méthode `health()` qui nous retourne le nombre de points de vie courant d'un personnage.
* Souvenez-vous: résolvez le problème de la manière la plus simple possible, n'anticipez pas sur les tests à venir.
* Pensez à refactor lorsque vos tests sont verts.
* Pensez à commiter à la fin de chaque Etape (voir plusieurs commits par Etape)

### Etape 2 

#### Le personnage...

- Peut être vivant ou mort.
- Peut faire des dégats à un autre personnage.

#### Conditions 

- Lorsque les dégats reçus sont supérieurs ou égaux au nombre de point de vie actuels, les points de vie tombent à 0 et le personnage meurt.

#### Tips

- Vous pouvez, par exemple, créer une méthode `void hit(Character opponent)` et partir du principe que chaque fois que cette méthode est appelée, elle va faire 10 dégats.
- Vous pouvez avoir besoin d'une méthode `isAlive()` afin de savoir si le personnage est vivant ou mort.
- Vous allez certainement avoir besoin de plusieurs personnages dans vos tests unitaires.


### Etape 3

Le personnage peut se soigner.

#### Conditions

- Lorsque le personnage est mort, il ne peut pas se soigner.
- Un personnage ne peut pas dépasser les 100 points de vie (même après un soin).

#### Tips

- Vous pouvez créer une méthode `void heal()` et partir du principe que chaque fois que cette méthode est appelée, elle va soigner 10 points de vie.
- Pour vos tests, il va falloir que votre personnage prenne des dégats avant d'avoir besoin de se soigner :)

### Etape 4

#### Le personnage ...

- peut appartenir à une faction.
- il peut rejoindre ou quitter une ou plusieurs factions.
- les personnages d'une même faction sont alliés : 
  - ils ne peuvent pas se faire de dégats entre eux.
  - ils peuvent se soigner entre eux.
  
#### Tips
 
 - Vous pouvez créer les méthodes `joinFaction(Faction faction)` & `leaveFaction(Faction faction)` qui permettront de rejoindre ou de quitter une faction.
 - Vous allez peut-être devoir modifier certains tests déjà écrits, car une nouvelle notion est apparue.
  
### Etape 5

Le personnage a une distance d'attaque.
- si c'est un combattant corps à corps (melee figher) il a une distance d'attaque de 2 mètres max.
- si c'est un combattant distance (ranged fighter) il a une distance d'attaque de 20 mètres max.
- lorsque le personnage fait des dégats, l'adversaire doit être à distance.
 
#### Tips

- Il va falloir ajouter l'information concernant le type de combattant (`melee` ou `ranged`)
