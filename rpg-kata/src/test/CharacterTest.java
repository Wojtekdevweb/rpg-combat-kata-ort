import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CharacterTest {

    Character JoueurA;
    Character JoueurB; 
    Character JoueurC; 

    @Before
    public void init() {
        JoueurA = new Character();
        JoueurB = new Character(); 
    }

    @Test
    public void vieEgaleA100() {
        assertThat(JoueurA.getVieMax()).isEqualTo(100);
    }

    @Test 
    public void joueurEnVie() {
        assertThat(JoueurA.estEnVie()).isEqualTo(true); 
    }

    @Test 
    public void joueurMort() {
        // WHEN 
        JoueurA.setVieMax(0);
        // THEN 
        assertThat(JoueurA.estEnVie()).isEqualTo(false); 
    }

    @Test 
    public void joueurInfligeDegat() {
        //WHEN 
        JoueurA.degat(JoueurB); 
        //THEN 
        assertThat(JoueurB.getVieMax()).isEqualTo(90); 
    }

    @Test 
    public void joueurSeSoigne() {
        JoueurA.degat(JoueurB); 
        JoueurA.heal(); 
        assertThat(JoueurA.getVieMax().isEqualTo(100)); 
    }

    @Test 
    public void joueurNePeutSeSoigne() {
        //WHEN 
        JoueurA.setVieMax(0); 
        JoueurA.heal(); 
        //THEN 
        assertThat(JoueurA.getVieMax().isEqualTo(0)); 
    }

    @Test 
    public void joueurNePeutAjouterPlusDe100PV() {
        //WHEN 
        JoueurB.heal(); 
        //THEN 
        assertThat(joueursB.getVieMax().isEqualTo(100)); 
    }

    @Test 
    public void joueurRejoinFaction() {
        //GIVEN 
        Faction faction = new Faction(); 
        //WHEN 
        JoueurA.joinFaction(faction); 
        boolean inFaction = JoueurA.isInFaction(); 
        //THEN
        assertThat(inFaction).isEqualTo(true); 
    }

    @Test 
    public void joueurQuitteFaction() {
        //Given 
        Faction faction = new Faction(); 
        //WHEN 
        JoueurA.joinFaction(faction); 
        JoueurB.leaveFaction(faction); 
        boolean inFaction = JoueurA.isInFaction(); 
        //THEN 
        assertThat(inFaction).isEqualTo(false); 
    }
    
    @Test
    public void joueurNePeutPasInfligerDesDegatsDansLaMemeFaction() {
        //GIVEN 
        Faction faction = new Faction(); 
        //WHEN 
        JoueurA.joinFaction(faction); 
        JoueurB.jointFaction(faction); 
        JoueurB.degat(JoueurA); 
        // THEN  
        assertThat(joueurA.getVieMax().isEqualTo(100)); 
    }

    @Test 
    public void JoueurSoigneJoueurMemeFaction() {
        //GIVEN 
        Faction faction = new Faction(); 
        // WHEN 
        JoueurA.joinFaction(faction); 
        JoueurA.degat(JoueurB); 
        JoueurB.joinFaction(faction); 
        JoueurA.heal(JoueurB); 
        // THEN 
        assertThat(joueurB.getVieMax().isEqualTo(100)); 
    }

    @Test
    public void joueurNePeutDamageJoueurMemeFaction() {
        // GIVEN
        Faction faction = new Faction();
        // WHEN
        JoueurA.joinFaction(faction);
        JoueurB.joinFaction(faction);
        JoueurB.hit(JoueurA);
        // THEN
        assertThat(joueurA.getVieMax().isEqualTo(100)); 
    }


}


