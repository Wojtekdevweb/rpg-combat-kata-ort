public class Character {
    private int vieMax; 
    private ArrayList<Faction> faction;

    public Character() {
        vieMax = 100; 
    }

    public int getVieMax() {
        return vieMax;
    }

    public void setVieMax(int newVieMax) {
        vieMax = newVieMax; 
    }

    public boolean estEnVie() {
        if(vieMax > 0) {
            return true; 
        } else {
            return false; 
        }
    }

    public void degat(Character adversaire) {
        if(adversaire.getVieMax() < 10) {
            adversaire.setVieMax(0);
        }
        else {
            adversaire.setVieMax(adversaire.getVieMax() - 10);
        }
    }

    public void heal() {
        if(this.estEnVie()) {
            if((this.getVieMax() + 10 ) > 100) {
                this.setVieMax(100);
            }
            else {
                this.setVieMax(this.getVieMax() + 10);
            }
        } 
    }

    public ArrayList<Faction> getFaction() {
        return faction;
    }

    public void setFaction(ArrayList<Faction> faction) {
        this.faction = faction;
    }
    public void joinFaction(Faction faction) {
        this.faction.add(faction);
    }

    public void leaveFaction(Faction faction) {
        this.faction.remove(faction);
    }

    public boolean isInFaction() {
        return (!faction.isEmpty());
    }


}