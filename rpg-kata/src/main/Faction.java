public class Faction {
    private String nom;

    public Faction() {
        nom = "Spariate Team";
    }

    public Faction(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

}
